# `git` going with version control

## Goal:

To familiarize yourself with version control software.


## Directions:

Choose between assignment A (hard), or assignment B (easy), below. Then proceed
to work on your project, making sure you keep track of your changes using
`git`. Make sure your code is available for the public (_e.g.,_ the grader, your
TA, or myself) to download by the time this quarter is over: for instance, you
can host your code on _bitbucket_, _github_, _gitlab_, or even the _laguna_
(pic) server (we can help you get set).

> **Important:** Whether or not your project "works as expected" is irrelevant
> for this assignment. You will be graded solely on your [proper, or improper]
> use of git. If you so choose, you can work on a completely different project
> (say a website, or a python program), and/or use a completely different
> version control software (just as long as you notify us in advance if you
> intend to follow this approach.


### Assignment A

Your very own `vector<ItemType>` class.

 *  [Click here for the assignment description][bb-link].
 *  [Click here to download a zip file][bb-link-zip] with "a copy" of the
    assignment description, as well as other companion files.

[bb-link]: https://bitbucket.org/rikis-salazar/10b-hw-pic10b-vector
[bb-link-zip]: https://bitbucket.org/rikis-salazar/10b-hw-pic10b-vector/downloads/


### Assignment B

Your job is to write a single-player version of the card game known as _'siete y
medio'_ (seven and a half), which is very similar in nature to the casino game
of Blackjack (also known as 21). The goal of the game is to get cards whose
total value comes the closest to 7&frac12; without going over it. Getting a card
total over 7&frac12; is called "busting". 

When a player makes a bet against the dealer. There are 4 possible outcomes:

 *  The player comes closer to 7&frac12; than the dealer or the dealer busts but
    the player did not bust. In this case the player wins the round and the
    player's money increases by the amount that was bet.
 *  The dealer comes closer to 7&frac12; than the player, or the player
    busts. In this case the player loses the round and the player's money
    decreases by the amount that was bet.
 *  Both, the player and dealer bust. In this case the player loses the round
    and the player's money decreases by the amount that was bet. This is called
    _house advantage_.
 *  Both the player and the dealer have the same total and they do not bust. In
    this case a tie is declared and no money is exchanged.


#### The mechanics of the game

The game is played with a _'Spanish baraja'._

| ![naipes.png](naipes.png) |  
|:-----:|  
| Figure 1: _Baraja espa&ntilde;ola_ |  


From the [Spanish playing cards][wiki-baraja] entry of wikipedia:

[wiki-baraja]: http://en.wikipedia.org/wiki/Spanish_playing_cards

> "The traditional Spanish baraja is an old deck that was brought over by the
> Moors to Spain during the 14th century. The cards are still called naipes
> after the n&#257;'ib cards found in the Mamluk deck... 
>
> Smaller decks have 40 cards and lack ranks 8, 9 and comodines (jokers). The
> four suits are **bastos** (_clubs_), **oros** (literally "_golds_", that is,
> _golden coins_), **copas** (_cups_), and **espadas** (_swords_). The four
> suits are thought to represent the four social classes of the Middle Ages. The
> suit of coins represents the merchants, the clubs represents the peasants, the
> cups represent the church and the swords represent the military.
>
> The three face cards of each suit have pictures similar to the _jack_,
> _queen_, and _king_ in the French deck, and rank identically. They are the
> **sota**, which is similar to the _jack/knave_ and generally depicts a _page_
> or _squire_, the **caballo** (_knight_, literally "_horse_"), and the
> **rey** (_king_) respectively."

At each round of play the player places a bet. After that, the player and the
dealer each draw one card from the deck.The player is then repeatedly asked if
another card is wanted. The player can continue to draw cards while the total is
less than 7&frac12;. After the player's turn is over, the dealer's cards are
shown. The dealer's play is always the same: the dealer continues to draw cards
only if the total is less than 5&frac12;.  The value of every card doesn't
always agree with the rank of the card. The values are listed in the following
table:

| **Name:** | As | Dos | Tres | Cuatro | Cinco | Seis | Siete | Sota | Caballo | Rey |
|:-------|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| **Rank:** | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 10 | 11 | 12 |
| **Values:** | 1 | 2 | 3 | 4 | 5 | 6 | 7 | &frac12; | &frac12; | &frac12; |


For this assignment, assume the player starts with 100 pesos (or dollars) and
the game ends when the player is down to 0, or when the dealer loses 900. Also,
it is a good idea to add input validation and prevent the user from betting an
amount bigger than the one the player actually has.

### What is your program expected to do?

Your program is expected to

 1. _play 'siete y medio' against you_, and
 2. _keep a log of the rounds you played against the dealer_.

When you run your program, important information about every round should be
recorded in the file `gamelog.txt`. Some sample runs of my program can be found
in the links below.

 *  [Sample run #1][sample-one] and its [companion log file][log-one].
 *  [Sample run #2][sample-two] and its [companion log file][log-two].

[sample-one]: sample_run_1.txt
[sample-two]: sample_run_2.txt
[log-one]: gamelog1.txt
[log-two]: gamelog2.txt


### What are you expected to do?

You are expected to use three classes: `Player`, `Card`, and `Hand`. You should
put the 3 class declarations in the header file `cards.h` and all member
function definitions in `cards.cpp`. You should write your main routine which
plays the seven-and-a-half game in the source file `siete-y-medio.cpp`. The
following templates should help you with the set up of your project.

 *  [cards.h][cards-h]: a complete interface for the `Card` class plus templates
    for the `Hand` and `Player` interfaces.
 *  [cards.cpp][cards-cpp]: an almost complete implementation of the `Card`
    class.
 *  [siete-y-medio.cpp][siete-cpp]: some suggested libraries plus a stub for the
    main routine.

[cards-h]: cards.h
[cards-cpp]: cards.cpp
[siete-cpp]: siete-y-medio.cpp

The `Card` class is provided to you but is incomplete, it is missing the
implementation of a couple of functions. You are to provide the interface as
well as the implementation of the member functions in the `Player` and `Hand`
classes. You can use as many member functions as you deem necessary and you are
free to decide how to organize the fields of these two classes. The `Player`
class should be pretty straightforward, you just need to keep track of the
amount of money a player has. You should be able to initialize, access and
update said amount.


## Submission

Sign up for either a [github][github], [gitlab][gitlab], or [bitbucket][bb]
account and host your source code in a public repository. Then [upload to
CCLE][ccle-hw] a simple text file (e.g., `my_repo.txt`) containing the URL of
this repository. The file will be automatically **collected at the date and
time listed in the CCLE assignment description** ("submission status" table
located at the bottom of the page).

[github]: https://github.com/
[gitlab]: https://about.gitlab.com/
[bb]: https://bitbucket.org/
[ccle-hw]: https://ccle.ucla.edu/course/view/19W-COMPTNG10C-2?section=2


### Grading rubric

Points will be assigned based on the following categories:

| **Category** | **Description** | **Points** |
|:-----|:----------------------------------------------------------|:---:|
| `readme.md` | Description of the project including documentation and/or test cases. See [this document][readme-example] for an example of a `readme.md` file. | 4 |
| Commits | You should work in increments and document your work. Try to be descriptive but concise when writing commit messages. For this project you should have at least 20 commit messages. | 4 |
| Branching | You are free to create as many branches as you see fit, but at the very minimum there should be an `experimental` branch where you test features before merging them into your `master` branch. | 4 |
| Merging | At some point in your development history you should merge two branches (_e.g._ `master` and `experimental`). Make sure **at least one of these merges is a non-fast-forward one**. | 4 |
| Other | This is left open for you, topics you might want to research are: _conflict resolutions_, _three-way merging_, creating `gitignore` files, etc. | 2 |
| Binary vs text | Version control software excels in keeping track on contents stored in text files. If your repository contains binary files (_e.g._ `.exe`, `.dll`, `.bin`, etc) there should be a very compelling reason for you to keep track of them. If this is the case you should document this reason in the `readme.md` file; otherwise you should `git rm` these files as soon as you become aware of their existence. | 2 |

[readme-example]: https://github.com/mr-mig/every-programmer-should-know

---

[Return to main course website][PIC]

[PIC]: ../..
