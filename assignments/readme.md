# Homework assignments

This is the place to find homework assignment descriptions. Please note that you
will not be able to submit your code via this setup. Instead every assignment
will contain a link to its corresponding CCLE assignment submission entry.

*   [Assignment 1: _Version control_][HW1].
*   [Assignment 2: _A `Qt` grade calculator_][HW2].
*   [Assignment 3: _A ring queue_][HW3].

[HW1]: hw1/
[HW2]: hw2/
[HW3]: hw3/


---

[Return to main course website][PIC]

[PIC]: ..

