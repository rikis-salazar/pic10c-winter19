# Final project

## What is this project about?

In short, it is about you. The basic idea is that you are more likely to learn
about something that you care. While it is relatively easy for me to have you
work on standard C++ related project, I am willing to bet that it will be more
enjoyable for you to set your own directions, and reach the milestones that make
the most sense to you.

## Ok, but... are there any 

Not quite! I'm still going to assign you a score based of how much of a learning
experience your project reflects. well I believe you
ha

assigned you_. This project is rather open, however in the previous experience
dictates that most projects will fall into roughly three categories.

1. Gethpast some broad
1. categories have manifested itslef

behind it is that you will spend more time working on a project that
you specifically chose because you care about it


## Directions:

Choose a particular topic/project you want to learn about and/or showcase. By
design this assignment is open to pretty much whatever you want to 

> **Important:** Whether or not your project "works as expected" is irrelevant
> for this assignment. You will be graded solely on your [proper, or improper]
> use of git. If you so choose, you can work on a completely different project
> (say a website, or a python program), and/or use a completely different
> version control software (just as long as you notify us in advance if you
> intend to follow this approach.


### What are you expected to do?

You are expected to use three classes: `Player`, `Card`, and `Hand`. You should
put the 3 class declarations in the header file `cards.h` and all member
function definitions in `cards.cpp`. You should write your main routine which
plays the seven-and-a-half game in the source file `siete-y-medio.cpp`. The
following templates should help you with the set up of your project.

The `Card` class is provided to you but is incomplete, it is missing the
implementation of a couple of functions. You are to provide the interface as
well as the implementation of the member functions in the `Player` and `Hand`
classes. You can use as many member functions as you deem necessary and you are
free to decide how to organize the fields of these two classes. The `Player`
class should be pretty straightforward, you just need to keep track of the
amount of money a player has. You should be able to initialize, access and
update said amount.


## Submission

Sign up for either a [github][github], [gitlab][gitlab], or [bitbucket][bb]
account and host your source code in a public repository. Then [upload to
CCLE][ccle-hw] a simple text file (e.g., `my_repo.txt`) containing the URL of
this repository. The file will be automatically **collected at the date and
time listed in the CCLE assignment description** ("submission status" table
located at the bottom of the page).

[github]: https://github.com/
[gitlab]: https://about.gitlab.com/
[bb]: https://bitbucket.org/
[ccle-hw]: https://ccle.ucla.edu/course/view/19W-COMPTNG10C-2?section=2


### Grading rubric


---

[Return to main course website][PIC]

[PIC]: ../..
