# Bitbucket repositories

Here you will find links to the bitbucket repositories that we have discussed
during lecture. Some of them are _better_ than others in the sense that they
include not only code, but also a companion set of of handouts.

> Please note that you can also [browse/search all my public
> repositories][other-repos]; if you choose to do so, you might find
> repeated/overlapping information that might help you get a better
> understanding of a specific topic.

[other-repos]: https://bitbucket.org/rikis-salazar/


*   [IntroGit-Winter19][repo1]: a simple repository we discuss/illustrate the
    basics behind version control (`git`).

*   [The C++ STL: Containers][repo2]: a markdown document _converted_ to two
    different `.pdf` documents. It presents some basic facts about C++
    containers in general.

*   [Inheritance in C++][repo3]: a collection of C++ examples that illustrate
    various concepts associated to inheritance and polymorphism.

*   [Function pointers in C++][repo4]: a collection of C++ examples that
    illustrate concepts associated to function pointers. The goal here is to get
    us used to the idea that it is possible to _pass a function as a parameter
    to another function_; this in turn helps us set a specific policy for the
    receiving function.

*   [The C++ STL: Templates][repo5]: a collection of C++ examples that
    illustrate the use of templates.

*   [Qt via CLI][repo6]: A simple project that was overly complicated by the use
of _primitive?!_ tools (_e.g.,_ `qmake`, `make`, etc.).

*   [User defined slots and signals in Qt][repo7]: Another ~~stupid~~ simple
    project that illustrates the way "functions communicate" in Qt.

*   [A _cute_ game][repo8]: The beginnings of a basic game.

*   [Memory management in C++][repo9]: Some common mistakes to be aware of, plus
    a revisit to the big 4.

*   [Smart pointers (_home-made_ &_standard_)][repo10]: Basic ideas behind
    reference counting, plus advantages and disadvantages of _standard_ smart
    pointers.

*   [The C++ STL: Iterators][repo11]: Why using iterators makes your code more
    _generic_. Classification of iterators depending on the operations they
    are allowed to perform.

[repo1]: https://bitbucket.org/rikis-salazar/introgit-winter19
[repo2]: https://bitbucket.org/rikis-salazar/10c-stl-containers/src
[repo3]: https://bitbucket.org/rikis-salazar/10c-review-inheritance/src
[repo4]: https://bitbucket.org/rikis-salazar/10c-review-function-pointers/src
[repo5]: https://bitbucket.org/rikis-salazar/10c-templates/src
[repo6]: https://bitbucket.org/rikis-salazar/10c-qt-via-cli/commits/all
[repo7]: https://bitbucket.org/rikis-salazar/10c-qt_connections/commits/all
[repo8]: https://bitbucket.org/rikis-salazar/10c-qt_dots/commits/all
[repo9]: https://bitbucket.org/rikis-salazar/10c-memorymanagement/src
[repo10]: https://bitbucket.org/rikis-salazar/10c-smart-pointers/commits/all
[repo11]: https://bitbucket.org/rikis-salazar/10c-iterators/

---

[Return to main course website][PIC]

[PIC]: ..

