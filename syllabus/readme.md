# Syllabus: Winter 2019 Lecture 2

> If you are reading the online (html) version of this syllabus, [click
> here][pdf] to access a `.pdf` version of this document.[^if-you]

[pdf]: syllabus.pdf
[^if-you]: The embedded link does not work in the `.pdf` document.

---

## Course information

**Contact:** [`rsalazar@math.ucla.edu`][correo] (write "Pic 10C" in the
subject).

**Time:** 12:00 to 12:50 pm.

**Location:** MS 6201.

**Office hours:** [See CCLE website][CCLE-website].

**Teaching assistant(s):**

|**Section**| **T. A.**     |**Office**| **E-mail**                           |
|:---------:|:--------------|:--------:|:-------------------------------------|
| 2A        | Krieger, A. J. | MS 3969  | [`akrieger@math.ucla.edu`][t-a] |

[correo]: mailto:rsalazar@math.ucla.edu
[CCLE-website]: https://ccle.ucla.edu/course/view/19W-COMPTNG10C-2
[t-a]: mailto:akrieger@math.ucla.edu


## Course Description

_Miscellaneous topics in computer programming._ Roughly speaking, the course can
be broken into three different parts:

1.   `C++` topics.- the idea is to review/revisit and/or [re]introduce topics
     you might have studied in previous courses. These topics include, but are
     not limited to: _the big 3/4/5/6, inheritance, templates, [function]
     pointers, function objects, data structures, iterators, generic algorithms,
     binders & adapters, lambda closures, etc._
     
     > Since it is expected that some [or most] of you are already familiar with
     > a portion of these topics, the emphasis here will be placed on _how
     > things work under the hood_, as opposed to _how to use them in practice_.

1.   Tools for your final project.- creating/maintaining local/remote code
     repositories, _"visual" programming_ via the `Qt` framework.

     > We will simply _give you the basics_. It will be up to you to decide how
     > far into these topics you want to go.

1.   Developer tools in general.- _version control_ (`git`, `Markdown`), _build
     automation_ (`GNU Make`), and _markup file conversion_ (`pandoc`).


## Textbook

You are welcome to use your copy of Horstmann C. & Budd, T. A. Big `C++`, _2nd/3rd
Edition._ John Wiley and Sons, Inc. However, be advised that most of the
material presented during lecture can be found elsewhere. Whenever possible,
lecture notes and/or slides will be prepared, and if applicable, made available
to students.


## Course websites: PIC, CCLE and MyUCLA

During this quarter I will utilize three different websites:

*   [PIC (`www.pic.ucla.edu/~rsalazar/pic10c`)][pic]: This is to be considered
    the _main class website_. Examples, code, slides, handouts, assignment
    descriptions, etc., will be hosted here.
*   [CCLE][ccle]: for class announcements and homework submissions.
*   [MyUcla][myucla]: to report scores/grades.

[pic]: https://www.pic.ucla.edu/~rsalazar/pic10c
[ccle]: ccle.ucla.edu
[myucla]: my.ucla.edu

## Reaching me via email

_Please consult CCLE announcements as well as this syllabus before sending me an
email about a policy or procedure as your question(s) might already be answered
here._

If you feel that your question/issue has not been addressed in the documents
listed above, do feel free to send me a message, however keep the following in
mind:

*   I receive a high volume of messages throughout the day: it might be faster
    for you to get the information you seek if you reach out to me in person
    (say during O.H., or right before/after lecture).
    
*   Messages with special _keywords_ "skip" my inbox. Use this to your
    advantage: if you add `Pic 10c` to your subject line, your message will find
    its way into a special folder that I check periodically. This should reduce
    the time you have to wait before I reply to it.

*   Messages with special attachments, more specifically text files with
    specific file extensions (_e.g.,_ `.cpp`, `.h`, etc.), wind up in my _trash_
    folder.

    > This inconvenience brought to you by students (mostly from pic10a) that
    > think deadlines do not apply to them.


## Midterms

One fifty-minute midterm will be given on
**February 11 (Monday Week 6)**
from
**12:00 to 12:50 pm**
at
**MS 6201**.
_There will be absolutely no makeup midterms under any circumstances._

The midterm exam will be returned to you during discussion section and your TA
will go over the exam at that time. _Any questions regarding how the exam was
graded **must be submitted in writing** with your exam to the TA at the end of
section that day._ No regrade requests will be allowed afterwards regardless of
whether or not you attended section. Please get in touch with me if you
anticipate missing section due to a family emergency or a medical reason.

## Final Exam

The final exam will be given on
**Thursday, March 21**
from
**3:00 to 6:00 pm**
at
**TBA** (click [here][final-tba] for up-to-date information about the exam
location). **_Failure to take the final exam during this time will result in an
automatic F!_**

[final-tba]: https://sa.ucla.edu/ro/public/soc/Results/ClassDetail?term_cd=19W&subj_area_cd=COMPTNG&crs_catlg_no=0010C+++&class_id=157052210&class_no=+002++

Make sure to bring your UCLA ID card to every exam. You will not be allowed to
consult books, notes, the internet, digital media or another student's exam.
Please turn off and put away any electronic devices during the entire duration
of the exam.


## Grading

_Grading method:_ Assignments, Midterm Exam, Final Exam, and Final Project.

Your final score in the class will be the maximum of the following two grading
breakdowns:

|                                                                                    |
|:----------------------------------------------------------------------------------:|
| (15% Assignments) + (25% Midterm 1) + (30% Final Exam) + (35% Final Project)[^one] | 
| or                                                                                 |
| (15% Assignments) + (50% Final Exam) + (35% Final Project)                         |

[^one]: Please notice that the weights in this breakdown do not add up to 100%.

_Letter grades:_  

|                       |                      |                    |
|:----------------------|:---------------------|:-------------------|
|                       | A (93.33% or higher) | A- (90% -- 93.32%) |
| B+ (86.66% -- 89.99%) | B (83.33% -- 86.65%) | B- (80% -- 83.32%) |
| C+ (76.66% -- 79.99%) | C (73.33% -- 76.65%) | C- (70% -- 73.32%) |

The remaining grades will be assigned at my own discretion. Please DO NOT
request exceptions.

**All grades are final when filed by the instructor on the Final Grade Report.**


## Policies and procedures

Once a homework assignment is posted (CCLE) you will have at least two weeks to
complete it. The actual due date might change but it will always be available
in its corresponding CCLE assignment submission page. You are encouraged to
check CCLE on a regular basis to find out about changes. You will upload the
requested files before the deadline; failing to do so will result in your
submission receiving a _late_ mark. 

There will be about 5 homework assignments throughout the quarter. Please note
that since the number of assignments is low, and since their weight on your
final score is low as well, **your lowest homework score will not be dropped**.

_In an effort to be fair to all students, messages sent to my email address that
contain either `.cpp`, `.txt`, or  `.h` attachments will automatically be
deleted from my inbox._

You are responsible for naming your files correctly and you need to make sure
that you submit them through the proper channels. You are free to use your
favorite software but you should make an effort to test your code on multiple
different platforms if applicable. Keep in mint that the programming environment
I will use to _compile_ your projects might be considerably different from the
one you used to work on your project. For reference, the computer(s) I will use
during this quarter are equipped with
_Visual Studio 2017_,
as well as some recent version of
`g++`
and
`clang++`
(this latter compiler is the one that is used by _Xcode_). In addition to this
testing, you are highly encouraged to keep your source code (or at least the
code "living" in your _master branch_ of your repositories) in a clean state:
free of commented out blocks of code, no object files, nor IDE specific
configuration binary files, etc.

If  for some reason your code does not compile on any of the computers I have
available at the time I am grading, I will make my best effort to assign you a
fair score based on the code that I am able to review.

Scores on homework assignments and exams might appear on the CCLE website,
however, these scores will not be the official ones. Usually I ask the class
grader to partially grade some assignments after week 5, then upload these
grades to CCLE. At this point, if your score is less that 20/20, you should
attempt to fix it. **All of your assignments will be graded by myself after the
final exam.** These official scores as well as comments and/or observations
about your work will be reported to MyUCLA. 

You are encouraged to discuss aspects of the course with other students as well
as homework assignments with others in general terms (i.e., general ideas and
words are OK but code is NOT OK). If you need specific help with your programs
you may consult the TA or the instructor only. Do not copy or cite any part of
a program or document written by someone else (e.g., code found online).
Homework solutions are automatically monitored for copied code.


## Final project 

Your final project consists of a series of _'deliverables'_ starting at week 3.
You have 2 weeks to come up with an approved 6-week project. If by the end of
this time you have not settled on a project I will be happy to suggest one for
you. The idea behind this is to get you excited about the material. By now you
should have some expertise will likely be able to come up with a pretty cool
project. Just remember: _all projects need to be approved by me_.


## Accommodations

If you need any accommodations for a disability, please contact the UCLA
CAE[^two] [(`www.cae.ucla.edu`)][CAE]. Make sure to let me know as soon as
possible about necessary arrangements that need to be made.

[CAE]: http://www.cae.ucla.edu
[^two]: Center for Accessible Education

|                                                      |
|:----------------------------------------------------:|
| Course Syllabus Subject to Update by the Instructor. |

