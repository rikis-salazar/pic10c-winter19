# Spring 2018 Midterm Exam (KEY)

## Question 1

Consider the...

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
#include <iostream>

class Algo{
  private:
    int* theData;
    ...

  public:
    Algo( size_t n = 1 ) : ... { }
    Algo( const Algo& b ) : ...  { ... }
    Algo& operator=( const Algo& rhs ) = default;
    ~Algo() { ... }
};

int main(){
    {
        Algo thing1(3);
        Algo thing2 = Algo();
        thing2 = Algo();
    }
    ...
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

> **Solution**
>
> First, notice the _mix & match_ regarding the big 4: the programmer is
> providing code for the _default constructor_, as well as the _destructor_;
> yet, it is asking the compiler to fill in the details about the _assignment
> operator_. This is a recipe for trouble, as the compiler provided assignment
> operator leads to pointer sharing of heap memory.
>
> In `main()`, in theory, the first two lines within the local scope are calls
> to class constructors: the one parameter constructor in `Algo thing1(3);`, as
> well as the default and copy constructors in `Algo thing2 = Algo();`. Up to
> this point, things are OK.
>
> In the next line, three things occur:
>
> i.  first a temporary unnamed object `Algo()` is created; then
> i.  this object is _shallow_-assigned to the already existing object `thing2`;
>     and finally
> i.  the unnamed object is destroyed via the class destructor.
>
> In step ii., the internal pointer of `thing2` is given a memory address that
> is about to disappear. In step iii., this memory address is released by the
> unnamed object's destructor. This means that when `thing2` goes out of scope
> and its destructor is called, a double deletion will occur which will crash
> the program.
>
> To fix this, we can simply eliminate the statement that calls the assignment
> operator. Alternatively, we can rewrite it using a _copy & swap_ approach.


## Question 2

**Answer _TRUE_ or _FALSE_.**

You do not need to justify your answer.

A)  When using `Qt`, the `Q_OBJECT` macro must appear in the `private:` section
    of all class definitions, regardless of whether they declare their own
    signals and/or slots.

    > **False.** It is only needed if you plan on using custom signal/slots.
    > However, it is common pratice to place this macro in every user defined
    > class.

A)  A _function object_, or _functor_ in `C++` is any instance of a class where
    `operator()` has been overloaded.

    > **True.**

A)  The statement

    ~~~~~ {.cpp}
    void* (*non_void)(void,void*);
    ~~~~~

    is a valid declaration of a function pointer.

    > **False.** There are too many `void`s, aren't they?

A)  If a class inherits from a purely virtual class, it [derived class] cannot be
    instantiated. That is, objects belonging to this class cannot be created.

    > **True.** However, if the purely virtual functions are overriden then the
    > derived class is no longer purely virtual and can then be instantiated.

A)  Assuming you have two branches, `master` and `experimental`, in your local
    repository, and that you've just _commited_ your latest changes in `master`.
    The command

    ~~~~ {.bash}
    $ git branch experimental
    ~~~~~

    "moves" you to the `experimental` branch.

    > **False.** This command attempts to create a new `experimental` branch.
    > This fails because there is already a branch with that name. To switch
    > over to other branches use `git checkout name_of_branch`.

A) The `Qt` application framework provides a _garbage collector_; this is the
    reason one can write `new` statements without their corresponding `delete`
    statements.

    > **False.** The reason no `delete` statements are needed is because `Qt`
    > objects are RAII ready, and they _"clean up after themselves"_ when they
    > go out of scope.


## Question 3

Consider the program:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
#include <iostream>

template<typename T>
T project_onto_circle( T x, T y ){
    T r = x*x + y*y;

    return x /= r;
}

int main(){
    double dx = 4.0, dy = 3.0;

    double d = project_onto_circle(dx,dy);
    int i = project_onto_circle<int>(dx,dy);

    std::cout << d << '\t' << i << '\n';

    return 0;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A)  ... minimum set of assumptions...

    > The following functions need to be defined:
    >
    > i.  Copy constructor
    > i.  `operator*`, `operator+`, and `operator/=`.
    >
    > Neither the assignment operator [`operator=`], nor the division operator
    > [`operator/`] are needed. Why?

A)  ... output...

    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    > 0.16      0
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Question 4

Consider the classes `Cosa`, as well as `Thing`, given by:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Thing {
  public:
    Thing() {}
    explicit Thing( const Cosa& ) {}
    Cosa operator()( const Thing& t ) const { ... }
};

class Cosa {
  public:
    Cosa() {}
    Cosa( const Thing& ) {}
    Thing operator()( const Cosa& c ) const { ... }
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

_Note:_ both classes are almost identical, except for the `explicit` ...


**Indicate the correct statement or statements.**

You may assume...

---

> Before answering the questions let us analyze the [full] signature of the
> `operator()` functions. First, we have
>
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
> Cosa Thing::operator( const Thing& ) const ;
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
>
> This can be translated as: _a `Thing` takes a `Thing` as parameter_ (see
> code below)...
>
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
> ____ Thing::operator( _____ Thing& ) _____ ;
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
>
> _... and it returns a `Cosa`. It also does not affect either the fields of
> the parameter, nor the implicit object_ (see code below).
>
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
> Cosa _____::________( const _____& ) const ;
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
>
> Similarly, the signature
>
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
> Thing Cosa::operator( const Cosa& ) const ;
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
>
> can be interpreted as: _a `Cosa` takes a `Cosa` and returns a `Thing` while
> leaving all fields in their original state._
>
> One more thing to notice here is that if a function expects a `Cosa` object as
> parameter, it is allowed to receive a `Thing` object and convert it to a
> `Cosa`. However, conversions from `Cosa` objects to `Thing` objects are
> explicitly forbidden.
>
> Whit this info in mind we can now tackle the questions.

---

A)  The expression `Cosa c1;`

    i.  _DOES_ compile. It calls the default constructor for the `Cosa` class.
    i.  _DOES NOT_ compile. Instead one can use `Cosa c1();` to call the
        default constructor.

        > The answer is i.
        >
        > The statement `Cosa c1();` is interpreted by the compiler as an
        > attempt to declare a function `c1` that takes no parameters and
        > returns a `Cosa` object.

A)  Assuming `c` is a `Cosa` object, the expression `Cosa c2(c);` _DOES_ compile
    because

    i.  it is a call to the copy constructor for the `Cosa` class.
    i.  first, `c` is implicitly converted into a `Thing` object, then the
        `Cosa` constructor that takes a `Thing` object as parameter is called.

        > The answer is i.
        >
        > The copy constructor was not _"deleted"_ in this example.

A)  Assuming `c` is a `Cosa` object, the expression `auto mistery = c(c);`

    i.  _DOES_ compile and `mistery` holds a `Thing` object.
    i.  _DOES_ compile and `mistery` holds a `Cosa` object.
    i.  _DOES NOT_ compile, but `auto mistery = c( Thing(c) );` does.

        > The answer is i.
        >
        > A `Cosa` takes a `Cosa` and returns a `Thing`.

A)  Assuming  `t` and `c` are objects of type `Thing` and `Cosa`, respectively,
    the expresssion `auto mistery = c(t);`

    i.  _DOES_ compile and `mistery` holds a `Thing` object.
    i.  _DOES_ compile and `mistery` holds a `Cosa` object.
    i.  _DOES NOT_ compile, but `auto mistery = c( Cosa(t) );` does.

        > The answer is i.
        >
        > While we agreed that a `Cosa` takes a `Cosa` and returns a `Thing`,
        > notice that a `Thing` can be implicitly converted to a `Cosa`.

A)  Assuming  `t` and `c` are objects of type `Thing` and `Cosa`,
    respectively, the expresssion `auto mistery = t(c);`

    i.  _DOES_ compile and `mistery` holds a `Thing` object.
    i.  _DOES_ compile and `mistery` holds a `Cosa` object.
    i.  _DOES NOT_ compile, but `auto mistery = t( Thing(c) );` does.

        > The answer is iii. Moreover, `mistery` is a `Cosa`. Why???

In addition to the `Cosa` and `Thing` classes described above, consider the
following template function:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template<typename FUN_T, typename THING_T>
void template_fun( const FUN_T& fun, const THING_T& t ){
    fun(t);
    return;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


**Answer TRUE or FALSE. If the answer is false, briefly explain what is the
issue.** Assume `c`, and `t`, are objects of type `Cosa`, and `Thing`,
respectively.

_Note:_ The first two statements have been solved for you, take advantage of
this information.

F) The expression `template_fun(t,t);` compiles.

    > **TRUE**

F) The expression `template_fun(t,c);` does not compile.

    > **TRUE**

Your turn:

H) The expression `template_fun(c,t);` compiles.

    > **TRUE**

H) The expression `template_fun( c(t) , t );` compiles.

    > **TRUE**

H) The expression `template_fun( Thing()( c(t) ) , c );` compiles.

    > **TRUE**

H) The expression `template_fun( Cosa( c(c) ) , t(t) );` compiles.

    > **TRUE**

---

[Return to main course website][PIC]

[PIC]: ../..

