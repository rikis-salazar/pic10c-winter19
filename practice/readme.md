# Practice exams and other resources

This is the place to find practice material for midterm and final exams. Please
keep in mind that there is no guarantee actual exams will look like any of these
documents. However, they should give you a good idea of what to expect regarding
the form and length of a given exam.

Practice questions:

*   [Midterm][link1]: practice problems for the midterm.
*   [Midterm (KEY)][link1-key]: solutions to practice problems for the midterm.
*   [Final][link3]: practice problems for the final exam.
*   [Final (KEY)][link3-key]: solutions to practice problems for the final exam.

Old exam(s):

*   [Spring 2018 Midterm][link2]
*   [Spring 2018 Midterm (KEY)][link2-key]

[link1]: practice-midterm/
[link2]: midterm-spring18/
[link3]: practice-final/
[link1-key]: practice-midterm-key/
[link2-key]: midterm-spring18-key/
[link3-key]: practice-final-key/

---

[Return to main course website][PIC]

[PIC]: ..

