#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <iterator>


template <typename ForwardIterator>
ForwardIterator mode_in_range( ForwardIterator first, ForwardIterator last ){

    if ( first == last )  // Empty range. Return iter past last element.
        return last;

    // There is at least one value. Choose it as candidate for `the_mode`.
    auto the_mode = first;
    int max_freq = 1;
    do {
        int current_freq = 0;
        for ( auto iter = first ; iter != last ; ++iter )
            if ( *iter == *first )
                ++current_freq;

        if ( max_freq < current_freq ) {
            max_freq = current_freq;
            the_mode = first;
        }

    } while( ++first != last );

    return the_mode;
}


int mode_in_vector( const std::vector<int>& v, int& t ){
    if ( v.size() == 0 ){
        t = 0;
        return v[0]; // Or whatever!!!
    }

    int max_frequency = 1;
    int the_mode = v[0];

    for ( int i = 0 ; i < v.size() ; ++i ){
        int current_value = v[i];
        int current_frequency = 1;

        for ( int j = i + 1 ; j < v.size() ; ++j ){
            if ( current_value == v[j] )
                ++current_frequency;
        }

        if ( current_frequency > max_frequency ){
            max_frequency = current_frequency;
            the_mode = current_value;
        }

    }

    t = max_frequency;
    return the_mode;
}

int main(){

    using std::list;
    using std::vector;
    using std::begin;
    using std::end;
    using std::rbegin;
    using std::rend;
    using std::cout;

    vector<int> v1 = {8, 2, 2, 8, 2};
    vector<int> v2 = {0, 0, 0, 7, 1, 1, 0, 1, 1};

    int t1, t2;
    int m1 = mode_in_vector( v1, t1 );
    int m2 = mode_in_vector( v2, t2 );

    cout << m1 << " appears " << t1 << " times.\n";
    cout << m2 << " appears " << t2 << " times.\n";
    cout << '\n';


    char a[] = {'+', '*' ,'+' ,'+' ,'*' ,'+' ,'*' ,'-' ,'*' ,'*'};
    list<int> l1( rbegin(v2), rend(v2) );  // v2 backwards
    list<double> l2 = {1.1, 1.1, 0, 1.1, 1.1, 7.7, 0, 0, 0};

    auto mode_a = *mode_in_range( begin(a), end(a) );
    auto mode_v2 = *mode_in_range( begin(v2), end(v2) );
    auto mode_l1 = *mode_in_range( begin(l1), end(l1) );
    auto mode_l2 = *mode_in_range( begin(l2), end(l2) );
    auto other_mode_l2 = *mode_in_range( rbegin(l2), rend(l2) );

    cout << "The mode in a is " << mode_a << ".\n";
    cout << "The mode in v2 is " << mode_v2 << ".\n";
    cout << "The mode in l1 is " << mode_l1 << ".\n";
    cout << "The mode in l2 is " << mode_l2 << ".\n";
    cout << "The mode in l2 is also " << other_mode_l2 << ".\n";

    return 0;
}
