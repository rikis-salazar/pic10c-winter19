# Spring 2018 Midterm Exam

## Question 1

Consider the following program:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
#include <iostream>

class Algo{       // Algo = Something in Spanish
  private:
    int* theData;
    size_t theSize;

  public:
    Algo( size_t n = 1 ) : theSize(n), theData( new int[n] ) { }

    Algo( const Algo& b ) : theSize( b.theSize ) {
        theData = new int[theSize];
        for ( size_t i = 0 ; i < theSize ; ++i )
            theData[i] = b.theData[i];
    }

    Algo& operator=( const Algo& rhs ) = default;

    ~Algo() { delete[] theData; }
};

int main(){
    { // <-- not a typo
        Algo thing1(3);
        Algo thing2 = Algo();
        thing2 = Algo();
    } // <-- end of local scope

    std::cout << "Help! I want to see the light!!!\n";
    return 0;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As the `cout` statement [indirectly] implies, the program _does compile_ but
it _crashes_ before the message is displayed. Explain why this happens and
describe (no code needed) a possible way to make the crash go away.

_Note:_ There are several possible correct answers. You will earn full credit
if your explanation makes sense, and the proposed changes make the program
display the message.


## Question 2

**Answer _TRUE_ or _FALSE_.**

You do not need to justify your answer.

A)  When using `Qt`, the `Q_OBJECT` macro must appear in the `private:` section
    of all class definitions, regardless of whether they declare their own
    signals and/or slots.

A)  A _function object_, or _functor_ in `C++` is any instance of a class where
    `operator()` has been overloaded.

A)  The statement

    ~~~~~ {.cpp}
    void* (*non_void)(void,void*);
    ~~~~~

    is a valid declaration of a function pointer.

A)  If a class inherits from a purely virtual class, it [derived class] cannot
    be instantiated. That is, objects belonging to this class cannot be created.

A)  Assuming you have two branches, `master` and `experimental`, in your local
    repository, and that you've just _commited_ your latest changes in `master`.
    The command

    ~~~~ {.bash}
    $ git branch experimental
    ~~~~~

    "moves" you to the `experimental` branch.

A)  The `Qt` application framework provides a _garbage collector_; this is the
    reason one can write `new` statements without their corresponding `delete`
    statements.


## Question 3

Consider the program:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
#include <iostream>

template<typename T>
T project_onto_circle( T x, T y ){
    T r = x*x + y*y;

    return x /= r;
}

int main(){
    double dx = 4.0, dy = 3.0;

    double d = project_onto_circle(dx,dy);
    int i = project_onto_circle<int>(dx,dy);

    std::cout << d << '\t' << i << '\n';

    return 0;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A)  Explain what is the minimum set of assumptions that are being placed on the
    template parameter `T`. In other words, list all the functions/operators
    that are being used in the template function `project_onto_circle`, that are
    supposed to, either be members of the class `T`, or take `T` objects as
    parameters.

A)  What is the output of the program?


## Question 4

Consider the classes `Cosa`, as well as `Thing`, given by:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Thing {
  public:
    // Default constuctor
    Thing() {}

    // Cosas are not implicit Things
    explicit Thing( const Cosa& ) {}

    // Translates Thing into Cosa. Note that the full signature is:
    //     Cosa Thing::operator()( const Thing& ) const;
    Cosa operator()( const Thing& t ) const {
        return Cosa(t);
    }
};

// Cosa = Thing in Spanish
class Cosa {
  public:
    // Default constuctor
    Cosa() {}

    // Things are implicit Cosas
    Cosa( const Thing& ) {}

    // Translates Cosa into Thing. Note that the full signature is:
    //     Thing Cosa::operator()( const Cosa& ) const;
    Thing operator()( const Cosa& c ) const {
        return Thing(c);
    }
};
// Since Cosa is defined in terms of Thing, and vice versa, the code above does
// not compile the way it is presented. However, this can be fixed by first
// declaring, and then defining the member functions of these classes. I chose
// not to present the working version here in an effort to make the code more
// readable and [slightly?] less confusing.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

_Note:_ both classes are almost identical, except for the `explicit` keyword in
one of the `Thing` constructors.


**Indicate the correct statement or statements.**

You may assume that all `C++` expressions are written within the `main()`
routine of a program that includes the classes `Thing` and `Cosa`, defined
above.

A)  The expression `Cosa c1;`

    i.  _DOES_ compile. It calls the default constructor for the `Cosa` class.
    i.  _DOES NOT_ compile. Instead one can use `Cosa c1();` to call the
        default constructor.

A)  Assuming `c` is a `Cosa` object, the expression `Cosa c2(c);` _DOES_ compile
    because

    i.  it is a call to the copy constructor for the `Cosa` class.
    i.  first, `c` is implicitly converted into a `Thing` object, then the
        `Cosa` constructor that takes a `Thing` object as parameter is called.

A)  Assuming `c` is a `Cosa` object, the expression `auto mistery = c(c);`

    i.  _DOES_ compile and `mistery` holds a `Thing` object.
    i.  _DOES_ compile and `mistery` holds a `Cosa` object.
    i.  _DOES NOT_ compile, but `auto mistery = c( Thing(c) );` does.

A)  Assuming  `t` and `c` are objects of type `Thing` and `Cosa`, respectively,
    the expresssion `auto mistery = c(t);`

    i.  _DOES_ compile and `mistery` holds a `Thing` object.
    i.  _DOES_ compile and `mistery` holds a `Cosa` object.
    i.  _DOES NOT_ compile, but `auto mistery = c( Cosa(t) );` does.

A)  Assuming  `t` and `c` are objects of type `Thing` and `Cosa`,
    respectively, the expresssion `auto mistery = t(c);`

    i.  _DOES_ compile and `mistery` holds a `Thing` object.
    i.  _DOES_ compile and `mistery` holds a `Cosa` object.
    i.  _DOES NOT_ compile, but `auto mistery = t( Thing(c) );` does.


In addition to the `Cosa` and `Thing` classes described above, consider the
following template function:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template<typename FUN_T, typename THING_T>
void template_fun( const FUN_T& fun, const THING_T& t ){
    fun(t);
    return;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


**Answer TRUE or FALSE. If the answer is false, briefly explain what is the
issue.** Assume `c`, and `t`, are objects of type `Cosa`, and `Thing`,
respectively.

_Note:_ The first two statements have been solved for you, take advantage of
this information.

F) The expression `template_fun(t,t);` compiles.

    > **TRUE**

F) The expression `template_fun(t,c);` does not compile.

    > **TRUE**

Your turn:

H) The expression `template_fun(c,t);` compiles.

H) The expression `template_fun( c(t) , t );` compiles.

H) The expression `template_fun( Thing()( c(t) ) , c );` compiles.

H) The expression `template_fun( Cosa( c(c) ) , t(t) );` compiles.

---

[Return to main course website][PIC]

[PIC]: ../..

